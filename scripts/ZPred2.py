import argparse
import re
import os
parser = argparse.ArgumentParser()
parser.add_argument('--filename', type=str, default='Test', help='Output filename')
parser.add_argument('--catalog', type=str, default='guadalupe_stats_Z0.5.csv', help='Catalog with labels train test split')

args = parser.parse_args()

def get_value(value):
    return re.findall(r"[-+]?(?:\d*\.\d+|\d+)", value)

feat = args.filename.split('_')
phys   = feat[0] 
lr     = get_value(feat[1])[0]
epochs = get_value(feat[2])[0]
batch  = get_value(feat[3])[0]
outsize = get_value(feat[4])[0]
dropout = get_value(feat[5])[0]
model   = feat[6]
label   = feat[7]
name    = args.filename[:-len(label)-1]

print("###########################")                                               
print(f'Physical Prediction: {phys}')                                              
print(f'Learning Rate: {lr}')                                                      
print(f'Epochs: {epochs}')                                                         
print(f'Batch Size: {batch}')                                                      
print(f'OutputChannels: {outsize}')                                                
print(f'Dropout: {dropout}')                                                       
print(f'Model Name: {model}')                                                      
print(f'Label: {label}')                                                           
print(f'Folder name: {name}')                                                      
print("###########################")                                                                                                                                  
os.makedirs(f'../model/deepgn/train/{name}/{label}', exist_ok=True)                                                                                                                   
command = f'sbatch --error ../model/deepgn/train/{name}/{label}/{name}.err --output ../model/deepgn/train/{name}/{label}/{name}.out /pscratch/sd/j/jfsuarez/DEEPGN/deepgn/scripts/ZPred{phys.title()}2.sh {lr} {epochs} {batch} {outsize} {dropout} {model} {name} {label} {args.catalog}'
print(command)                                                                    
os.system(command) 