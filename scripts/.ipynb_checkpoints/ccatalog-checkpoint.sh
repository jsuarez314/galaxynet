#!/bin/bash
#SBATCH -N 1
#SBATCH -C cpu
#SBATCH -q regular
#SBATCH -J ccatalog
#SBATCH --mail-user=jf.suarez@uniandes.edu.co
#SBATCH --mail-type=ALL
#SBATCH -t 12:00:00
#SBATCH -A desi

hostname
date

source /global/common/software/desi/desi_environment.sh master
export CONDA_ENVS_PATH=/global/homes/j/jfsuarez/.conda/envs/
source activate deepgn
export PYTHONPATH=/global/homes/j/jfsuarez/.conda/envs/deepgn/lib/python3.9/site-packages:${PYTHONPATH}

date
#run the application:
#applications may performance better with --gpu-bind=none instead of --gpu-bind=single:1 
srun -n 4 -c 64 --cpu_bind=cores python /pscratch/sd/j/jfsuarez/DEEPGN/deepgn/scripts/ccatalogmultiple.py --nodes 4

date