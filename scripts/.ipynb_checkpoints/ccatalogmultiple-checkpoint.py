import os
import numpy
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--nodes', type=int, default=0, help='Number of nodes')

args = parser.parse_args()

taskID = os.environ["SLURM_PROCID"]

command = f'/global/homes/j/jfsuarez/.conda/envs/deepgn/bin/python /pscratch/sd/j/jfsuarez/DEEPGN/deepgn/01_Create_Catalog_guada_jpg.py --release guadalupe --survey main --clss bgs --overwrite False --nodes {args.nodes} --block {taskID}'
print(command)
os.system(command)