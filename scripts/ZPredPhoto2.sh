#!/bin/bash
#SBATCH -N 1
#SBATCH -C gpu
#SBATCH -G 2
#SBATCH -q regular
#SBATCH -J deepgn2n
#SBATCH --mail-user=jf.suarez@uniandes.edu.co
#SBATCH --mail-type=ALL
#SBATCH -t 00:05:00
#SBATCH -A desi_g

#OpenMP settings:
export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

hostname
date

echo "/global/homes/j/jfsuarez/.conda/envs/deepgn/bin/python /pscratch/sd/j/jfsuarez/DEEPGN/deepgn/03_Regression_Photo2.py --lr $1 --epochs $2 --batch $3 --kernel $4 --dropout $5 --model $6 --name $7 --label $8 --catalog $9"

source /global/common/software/desi/desi_environment.sh master
export CONDA_ENVS_PATH=/global/homes/j/jfsuarez/.conda/envs/
source activate deepgn
export PYTHONPATH=/global/homes/j/jfsuarez/.conda/envs/deepgn/lib/python3.9/site-packages:${PYTHONPATH}

#run the application:
#applications may performance better with --gpu-bind=none instead of --gpu-bind=single:1 
srun -n 2 -c 64 --cpu_bind=cores -G 2 --gpu-bind=single:1 /global/homes/j/jfsuarez/.conda/envs/deepgn/bin/python  /pscratch/sd/j/jfsuarez/DEEPGN/deepgn/03_Regression_Photo2.py --lr $1 --epochs $2 --batch $3 --kernel $4 --dropout $5 --model $6 --name $7 --label $8 --catalog $9

date