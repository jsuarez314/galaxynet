#!/bin/bash
#SBATCH -N 1
#SBATCH -C gpu
#SBATCH -G 1
#SBATCH -q debug
#SBATCH -J deepgn
#SBATCH -t 00:01:00
#SBATCH -A desi_g

#module load pytorch
#source activate deepgn

#run the application:
#applications may performance better with --gpu-bind=none instead of --gpu-bind=single:1 
srun -n 1 -c 32 --cpu_bind=cores -G 1 --gpu-bind=none  /global/homes/j/jfsuarez/.conda/envs/deepgn/bin/python /pscratch/sd/j/jfsuarez/DEEPGN/deepgn/scripts/gputest.py