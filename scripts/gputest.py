import numpy as np
import pandas as pd
import os 
import argparse
import pylab as pl
from   glob import glob
from   PIL import Image
from   astropy.io import fits
from   astropy.table import Table

import torch

print("torch_version: ",torch.__version__)
print("numpy_version: ",np.__version__)

from torch            import nn
from torch            import FloatTensor
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torch.fx import symbolic_trace
from torch.nn         import functional as F
from torchvision      import transforms, models
from torchvision.models import vit_l_16,  ViT_L_16_Weights
from torchvision.models import vit_l_32,  ViT_L_32_Weights
from torchvision.models import swin_b, Swin_B_Weights
from torchvision.models import swin_s, Swin_S_Weights
from torchvision.models import swin_t, Swin_T_Weights

from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error

print(torch.cuda.is_available())

for i in range(torch.cuda.device_count()):
    print(torch.cuda.get_device_name(i))
    
print(torch.cuda.is_available())
device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
print(f'Device: {device}')