#!/bin/bash
#SBATCH -N 1
#SBATCH -C gpu
#SBATCH -G 1
#SBATCH -q regular
#SBATCH -J dgnresnet
#SBATCH --mail-user=jf.suarez@uniandes.edu.co
#SBATCH --mail-type=ALL
#SBATCH -t 12:00:00
#SBATCH -A desi_g

hostname
date

echo "/global/homes/j/jfsuarez/.conda/envs/deepgn/bin/python /pscratch/sd/j/jfsuarez/DEEPGN/deepgn/03_Regression_Photo_jpg.py --lr $1 --epochs $2 --batch $3 --lf $4 --dropout $5 --model $6 --name $7 --label $8 --catalog $9"


source /global/common/software/desi/desi_environment.sh master
export CONDA_ENVS_PATH=/global/homes/j/jfsuarez/.conda/envs/
source activate deepgn
export PYTHONPATH=/global/homes/j/jfsuarez/.conda/envs/deepgn/lib/python3.9/site-packages:${PYTHONPATH}

#run the application:
#applications may performance better with --gpu-bind=none instead of --gpu-bind=single:1 
srun -n 1 -c 1 --cpu_bind=cores -G 1 --gpu-bind=single:1  /global/homes/j/jfsuarez/.conda/envs/deepgn/bin/python  /pscratch/sd/j/jfsuarez/DEEPGN/deepgn/03_Regression_Photo_jpg.py --lr $1 --epochs $2 --batch $3 --lf $4 --dropout $5 --model $6 --name $7 --label $8 --catalog $9

date