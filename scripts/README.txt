# To train 
/global/homes/j/jfsuarez/.conda/envs/deepgn/bin/python ZPred.py --filename photo_lr0.1_e100_b32_lf6_d0.3_resnet50_Z0.6LF6Guada --catalog ./../../../data/deepgn/catalog_guadalupe_main_bgs_jpg/photo_features_full.csv



# To known total of fits files
set -- ./../../../data/deepgn/catalog_guadalupe_main_bgs_jpg/*/*/*.fits; printf 'There are %s .fits files\n' "$#"