import os

import numpy  as np
import pandas as pd

from   glob import glob
import fitsio

from   astropy.table import Table, vstack, join, unique
from   astropy.io import fits

import desispec.io
from   tqdm.notebook import tqdm

def photo_error(photoerr_file, sfile, band, tid, tileid, expid, target_ra, target_dec):
    # Function to write an error from the photometric data pushing proccess
    photoerror_files = pd.read_csv(photoerr_file)
    print(f'Error pulling photometric for {sfile} {band} band')
    photoerror_files = photoerror_files.append(pd.DataFrame([[sfile, band, tid, tileid, expid, target_ra, target_dec]],
                                                            columns=['file','band','targetid','tileid','expid','target_ra','target_dec']))
    photoerror_files.to_csv(photoerr_file, index=False)
    del photoerror_files

def create(release, survey, clss, phys_targets, night, tile, overwrite):
    #- Define the path of the data by release
    path = f'/global/cfs/cdirs/desi/spectro/redux/{release}/tiles/pernight'
    
    #- Define the output path
    outpath = f'../../data/deepgn/catalog_{release}_{survey}_{clss}'
    os.makedirs(f'{outpath}/spec/train/', exist_ok=True)
    os.makedirs(f'{outpath}/spec/test/', exist_ok=True)
    os.makedirs(f'{outpath}/spec/valid/', exist_ok=True)
    os.makedirs(f'{outpath}/photo/train/', exist_ok=True)
    os.makedirs(f'{outpath}/photo/test/', exist_ok=True)
    os.makedirs(f'{outpath}/photo/valid/', exist_ok=True)
    
    
    #- Save the sfiles completed
    cat_file      = f'./catalog_{release}_{survey}_{clss}_files.csv'
    
    #- Save the sfiles with spectra error
    specerr_file  = f'./catalog_{release}_{survey}_{clss}_specerr.csv'
    
    #- Save the sfiles with photo error
    photoerr_file = f'./catalog_{release}_{survey}_{clss}_photoerr.csv'
    
    #- If overwrite == 'yes' the catalog creation start from 0%
    if overwrite == 'yes':
        catalog_files = pd.DataFrame(columns=['file'])
        catalog_files.to_csv(cat_file, index=False)
        
        specerror_files = pd.DataFrame(columns=['file'])
        specerror_files.to_csv(specerr_file, index=False)
        
        photoerror_files = pd.DataFrame(columns=['file','band','targetid',
                                        'tileid','expid','target_ra','target_dec'])
        photoerror_files.to_csv(photoerr_file, index=False)
        
    #- Columns from the spectra files
    scolumns = ['TARGETID','NIGHT','TILEID','PETAL_LOC','EXPID','FIBER','MJD','TARGET_RA','TARGET_DEC','FIBERSTATUS',
                # 'FLUX_IVAR_B','FLUX_IVAR_R','FLUX_IVAR_Z','FLUX_IVAR_BRZ', 
                'FLUX_B','FLUX_R','FLUX_Z','FLUX_BRZ',
                'SUMFLUX_B','SUMFLUX_R','SUMFLUX_Z','SUMFLUX_BRZ']
    
    #- Columns from the zcatalog files
    zcolumns = ['TARGETID','Z','ZERR','SPECTYPE']

    #- Find the spectra files to iterate
    sfiles = np.sort(glob(f'{path}/{tile}/{night}/spectra*.fits'))

    #- Iterate between spectra files
    for sfile in sfiles[0:1]:
        
        #- Read the temporal file that contains the objects created
        catalog_files    = pd.read_csv(cat_file)

        #- Ignore if the spectrafile is into the catalog.csv file
        if sfile in list(catalog_files['file']):
            None
        else:
            try:
                #- Read Spectra file
                sp = desispec.io.read_spectra(sfile)
                sp_pd = sp.fibermap.to_pandas()

                #- Extract all the FLUX
                sp_pd['FLUX_B'] = [i for i in sp.flux['b']]
                sp_pd['FLUX_R'] = [i for i in sp.flux['r']]
                sp_pd['FLUX_Z'] = [i for i in sp.flux['z']]
                sp_pd['FLUX_BRZ'] = [np.concatenate([x,y,z])  for x,y,z in zip(sp.flux['b'], sp.flux['r'], sp.flux['z'])]       

                #- Concatenate IVAR
                # sp_pd['FLUX_IVAR_B'] = [i for i in sp.ivar['b']]
                # sp_pd['FLUX_IVAR_R'] = [i for i in sp.ivar['r']]
                # sp_pd['FLUX_IVAR_Z'] = [i for i in sp.ivar['z']]
                # sp_pd['FLUX_IVAR_BRZ'] = [np.concatenate([x,y,z])  for x,y,z in zip(sp.ivar['b'], sp.ivar['r'], sp.ivar['z'])]       

                #- Compute the sum of the flux to identify null flux
                sum_flux_b = [sum(i) for i in sp_pd['FLUX_B']]
                sum_flux_r = [sum(i) for i in sp_pd['FLUX_R']]
                sum_flux_z = [sum(i) for i in sp_pd['FLUX_Z']]
                sum_flux_brz = [sum(i) for i in sp_pd['FLUX_BRZ']]

                #- Save the sum flux
                sp_pd['SUMFLUX_B'] = sum_flux_b
                sp_pd['SUMFLUX_R'] = sum_flux_r
                sp_pd['SUMFLUX_Z'] = sum_flux_z
                sp_pd['SUMFLUX_BRZ'] = sum_flux_brz

                #- Read the Zcatalog file (or redrock)
                zfile = sfile.replace('spectra','redrock')
                zs = fitsio.read(zfile)

                s_temp = sp_pd[scolumns]
                z_temp = pd.DataFrame(zs[zcolumns].byteswap().newbyteorder(), columns=zcolumns)

                #- Merge Spectra and zcatalog file
                df_spec = pd.merge(s_temp, z_temp)

                #- Merge with PHYSICAL_TARGETS
                df_spec = pd.merge(df_spec, phys_targets, on=['TARGETID','TARGET_RA','TARGET_DEC','Z'])            

            # print(df_spec['FLUX_IVAR_B'])
            except:
                specerror_files  = pd.read_csv(specerr_file)
                print(f'Error reading spectra on {sfile}')
                #
                specerror_files = specerror_files.append(pd.DataFrame([sfile],
                                                              columns=['file']))
                specerror_files.to_csv(specerr_file, index=False)
                del specerror_files
                continue
                

            #- Ignore if df_spec is null 
            if len(df_spec) > 0:
                df_spec = df_spec[~df_spec.astype(str).duplicated()]
                
                #- Apply some filters
                df_spec = df_spec[df_spec['FIBERSTATUS'] == 0]
                df_spec = df_spec[df_spec['SUMFLUX_B'] != 0]
                df_spec = df_spec[df_spec['SUMFLUX_R'] != 0]
                df_spec = df_spec[df_spec['SUMFLUX_Z'] != 0]
                df_spec = df_spec[df_spec['SUMFLUX_BRZ'] != 0]
                df_spec = df_spec.drop(['SUMFLUX_B','SUMFLUX_R','SUMFLUX_Z','SUMFLUX_BRZ','FIBERSTATUS'], axis=1)

                wave_b = sp.wave['b']
                wave_r = sp.wave['r']
                wave_z = sp.wave['z']
                wave_brz = np.concatenate([sp.wave['b'],sp.wave['r'],sp.wave['z']])

                # pbar = tqdm(total=len(df_spec), desc=sfile)
                for i in range(len(df_spec)):
                    print(df_spec.keys())
                    tid        = np.array(df_spec['TARGETID'])[i]
                    target_ra  = np.array(df_spec['TARGET_RA'])[i]
                    target_dec = np.array(df_spec['TARGET_DEC'])[i]
                    tileid     = np.array(df_spec['TILEID'])[i]
                    fiber      = np.array(df_spec['FIBER'])[i]
                    petal_loc  = np.array(df_spec['PETAL_LOC'])[i]
                    expid      = np.array(df_spec['EXPID'])[i]
                    night      = np.array(df_spec['NIGHT'])[i]
                    mjd        = np.array(df_spec['MJD'])[i]
                    z          = np.array(df_spec['Z'])[i]
                    zerr       = np.array(df_spec['ZERR'])[i]
                    spectype   = np.array(df_spec['SPECTYPE'])[i]
                    flux_b     = np.array(df_spec['FLUX_B'])[i]
                    flux_r     = np.array(df_spec['FLUX_R'])[i]
                    flux_z     = np.array(df_spec['FLUX_Z'])[i]
                    flux_brz   = np.array(df_spec['FLUX_BRZ'])[i]
                    
                    mag_g        = np.array(df_spec['MAG_G'])[i]
                    mag_r        = np.array(df_spec['MAG_R'])[i]
                    mag_z        = np.array(df_spec['MAG_Z'])[i]
                    mag_w1       = np.array(df_spec['MAG_W1'])[i]
                    mag_w2       = np.array(df_spec['MAG_W2'])[i]
                    ratio_ba     = np.array(df_spec['RATIO_BA'])[i]
                    shape_r      = np.array(df_spec['SHAPE_R'])[i]
                    
                    label      = np.array(df_spec['LABEL'])[i]

                    phys_targets_keys = ['Z',                             
                                         'MAG_G', 'MAG_R', 'MAG_Z', 'MAG_W1', 'MAG_W2',
                                         'SHAPE_R',
                                         'RATIO_BA',
                                        ]
                    phys_features = np.array(df_spec[phys_targets_keys])[i]

                    if float(target_dec)>0:
                        filename = f'{outpath}/spec/{label}/DESI_{tid}_{target_ra}+{target_dec}.fits'
                    else:
                        filename = f'{outpath}/spec/{label}/DESI_{tid}_{target_ra}{target_dec}.fits'
                    
                    #- If the file exists, then do nothing. Else download the images and create the fits file
                    if os.path.isfile(filename):
                        None
                    else:
                        try:
                            command_g = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_g.fits --ra {target_ra} --dec {target_dec} --size 256 --layer ls-dr9 --pixscale 0.262 --band g --force'
                            os.system(command_g)
                            image_g = fits.open('temp_g.fits')[0].data
                            os.system('rm temp_g.fits')
                        except:
                            photo_error(photoerr_file, sfile, 'G', tid, tileid, expid, target_ra, target_dec)
                            continue
                                
                        try:
                            command_r = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_r.fits --ra {target_ra} --dec {target_dec} --size 256 --layer ls-dr9 --pixscale 0.262 --band r --force'
                            os.system(command_r)
                            image_r = fits.open('temp_r.fits')[0].data
                            os.system('rm temp_r.fits')
                        except:
                            photo_error(photoerr_file, sfile, 'R', tid, tileid, expid, target_ra, target_dec)
                            continue

                        try:
                            command_z = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_z.fits --ra {target_ra} --dec {target_dec} --size 256 --layer ls-dr9 --pixscale 0.262 --band z --force'
                            os.system(command_z)
                            image_z = fits.open('temp_z.fits')[0].data
                            os.system('rm temp_z.fits')
                        except:
                            photo_error(photoerr_file, sfile, 'Z', tid, tileid, expid, target_ra, target_dec)
                            continue
                            
                        try:
                            command_w1 = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_w1.fits --ra {target_ra} --dec {target_dec} --size 256 --layer unwise-neo7 --pixscale 0.262 --band W1 --force'
                            print(command_w1)
                            os.system(command_w1)
                            image_w1 = fits.open('temp_w1.fits')[0].data
                            os.system('rm temp_w1.fits')
                        except:
                            photo_error(photoerr_file, sfile, 'W1', tid, tileid, expid, target_ra, target_dec)
                            continue
                            
                        try:
                            command_w2 = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_w2.fits --ra {target_ra} --dec {target_dec} --size 256 --layer unwise-neo7 --pixscale 0.262 --band W2 --force'
                            print(command_w2)
                            os.system(command_w2)
                            image_w2 = fits.open('temp_w2.fits')[0].data
                            os.system('rm temp_w2.fits')
                        except:
                            photo_error(photoerr_file, sfile, 'W2', tid, tileid, expid, target_ra, target_dec)
                            continue

                        try:
                            command_grz = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_grz.fits --ra {target_ra} --dec {target_dec} --size 256 --layer ls-dr9 --pixscale 0.262 --band grz --force'
                            os.system(command_grz)
                            image_grz = fits.open('temp_grz.fits')[0].data
                            os.system('rm temp_grz.fits')
                        except:
                            photo_error(photoerr_file, sfile, 'GRZ', tid, tileid, expid, target_ra, target_dec)
                            continue
                            
                            
                        hdict = {'TARGETID':tid, 'TARGET_RA':target_ra, 'TARGET_DEC':target_dec,
                                'TILEID':tileid, 'FIBER':fiber, 'PETAL_LOC':petal_loc, 'EXPID':expid,
                                'NIGHT':night, 'MJD':mjd, 'Z':z, 'ZERR':zerr, 'SPECTYPE':spectype}
                        
                        data = {'FLUX_B':flux_b, 'WAVE_B':wave_b}
                        fitsio.write(filename, data, header=hdict)

                        data = {'FLUX_R':flux_r, 'WAVE_R':wave_r}
                        fitsio.write(filename, data, header=hdict)

                        data = {'FLUX_Z':flux_z, 'WAVE_Z':wave_z}
                        fitsio.write(filename, data, header=hdict)                

                        data = {'FLUX_BRZ':flux_brz, 'WAVE_BRZ':wave_brz}
                        fitsio.write(filename, data, header=hdict)

                        
                        
                        filename = filename.replace('spec','photo')
                        
                        hdict = {'TARGETID':tid, 'MAG_G':mag_g, 'MAG_R':mag_r, 'MAG_Z':mag_z, 'MAG_W1':mag_w1, 'MAG_W2':mag_w2,
                                 'SHAPE_R':shape_r, 'RATIO_BA':ratio_ba}
                        
                        data = {'PHOTO_G':image_g}
                        fitsio.write(filename, data, header=hdict)

                        data = {'PHOTO_R':image_r}
                        fitsio.write(filename, data, header=hdict)

                        data = {'PHOTO_Z':image_z}
                        fitsio.write(filename, data, header=hdict)
                        
                        data = {'PHOTO_GRZ':image_grz}
                        fitsio.write(filename, data, header=hdict)

                        data = {'PHOTO_W1':image_w1}
                        fitsio.write(filename, data, header=hdict)

                        data = {'PHOTO_W2':image_w2}
                        fitsio.write(filename, data, header=hdict)
                        


                        del image_g, image_r, image_z, image_w1, image_w2, image_grz
                    # pbar.update()
            catalog_files = catalog_files.append( pd.DataFrame([sfile], columns=['file']))
            catalog_files.to_csv(cat_file, index=False)

    return 0
