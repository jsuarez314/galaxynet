import os
import numpy as np

def list_files(startpath):
    """Print directory structure and files within
    Taken from https://stackoverflow.com/questions/9727673/list-directory-tree-structure-in-python
    """
    print(f'{startpath} structure and files:')

    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}/'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)
        for f in files:
            print('{}{}'.format(subindent, f))
    
def np_unstrcuted(A):
    A = np.array(A, copy=True)
    return A.view((float, (len(A.dtype.names))))

def metrics(Y_test, Y_pred, error):
    bias = (Y_pred - Y_test)/(1+Y_test)
    sigma_nmad = 1.4826*np.median(np.abs(bias - np.median(bias)))
    f_out = np.sum(np.abs(bias) > error)/len(Y_pred)*100
    return bias, sigma_nmad, f_out

def mw_transmission(ebv, band):
    coeffs = {'U':3.995, 'G': 3.214, 'R': 2.165, 'I': 1.592, 'Z': 1.211, 'Y': 1.064,
              'W1': 0.184, 'W2': 0.113, 'W3': 0.0241, 'W4': 0.00910}
    return 10**(coeffs[band]*ebv/-2.5)

def mag(flux, ebv, band):
    return 22.5 - 2.5*np.log10(flux/mw_transmission(ebv, band))

def ratioBA(E1,E2):
    E  = np.sqrt(E1**2 + E2**2)
    return (1-E)/(1+E)

  
  
from matplotlib.transforms import Affine2D
from mpl_toolkits.axisartist.floating_axes import FloatingSubplot
from mpl_toolkits.axisartist.floating_axes import GridHelperCurveLinear
from mpl_toolkits.axisartist import angle_helper
from mpl_toolkits.axisartist.grid_finder import MaxNLocator
from matplotlib.projections import PolarAxes
  
def cone(ra, z, scale=0.5, orientation='horizontal',
         raaxis='min', ralim=None, zlim=None, hms=False, 
         colors=None, classes=None, title=None, fontsize=15,
         plot=None, fig=None, subnum=None, 
         xlabel=r"$\alpha$", ylabel=r"$\mathsf{redshift}$", **kwargs):

    """
    Make a wedge plot of RA/Dec vs redshift z, where RA/DEC are in degrees

    Parameters
    ----------
    Input data

    angle : (n, ) array
            RA in degrees
    redshift : (n, ) array

    scale: 0.5

    orientation:
        'horizontal': increasing z along +ve xaxis
        'vertical': increasing z along +ve yaxis
        angle in degrees: increasing z along the tilted axis

    raxis: 'min' | 'mid' | float
        default is 'min'
        RA value along which the cone plot is orientated horizontal
        or vertical

    ralim, zlim: list [ramin, rmax], list [zmin, zmax]
        default is taken from the lower/upper bound of the input data

    scatter: any kwargs compatible with plt.scatter

    hms: show RA labels in units of hours (if True) or degrees (if False)

    lookbtime: True/False

    plot: None
         'scatter' | 'hexbin' etc
    fig: supply figure instance
         default None

    subnum: subplot number e.g. 111, 221 etc
            default None

    xlabel: r"$\alpha$"

    ylabel: r"$\mathsf{redshift}$"

    cosmology: dict
     Uses cosmolopy package to compute look-back time
     default cosmology is {'omega_M_0': 0.3,
                           'omega_lambda_0': 0.7,
                          'h': 0.72}

    kwargs
    ------
    scatter = {'s': 4, 'marker': ','}

    Notes
    -----
        --Decorations that can be done outside the code:
             Draw grids: ax.grid(True, alpha=0.2)
             Set title: ax.set_title('Wedge plot')

        --Look-back time as twin axis to redshift not yet implemented

        --In future plan is to able to put colorbar in the plot too.
          Using cmap option.
    """

    # ------ Extents of ra and z
    if ralim:
        ramin, ramax = ralim
    else:
        ramin, ramax = ra.min(), ra.max()

    if zlim:
        zmin, zmax = zlim
    else:
        zmin, zmax = z.min(), z.max()

    # ----- Scale and Orientation of the wedge
    if orientation == 'horizontal':
        dirn = 0.
    elif orientation == 'vertical':
        dirn = 90.
    else:
        dirn = orientation

    if raaxis == 'min':
        raaxis = ramin
    elif raaxis == 'mid':
        raaxis = 0.5*(ramin+ramax)

    # Tilt of a cone relative to minimum RA
    tr_rotate = Affine2D().translate(dirn/scale - raaxis, 0.0)

    # Scaling the opening angle
    tr_scale = Affine2D().scale(scale*np.pi/180., 1.0)

    tr = tr_rotate + tr_scale + PolarAxes.PolarTransform()

    # ---- Grids
    if hms is True:
        grid_locator1 = angle_helper.LocatorHMS(5.0)
        tick_formatter1 = angle_helper.FormatterHMS()
    else:
        grid_locator1 = angle_helper.LocatorDMS(5.0)
        tick_formatter1 = angle_helper.FormatterDMS()

    grid_locator2 = MaxNLocator(6)

    grid_helper = GridHelperCurveLinear(tr,
                                        extremes=(ramin, ramax, zmin, zmax),
                                        grid_locator1=grid_locator1,
                                        grid_locator2=grid_locator2,
                                        tick_formatter1=tick_formatter1,
                                        tick_formatter2=None)

    # Figure properties
    if not fig:
        fig = plt.figure(figsize=(8, 7))
        subnum = 111
    ax = FloatingSubplot(fig, subnum, grid_helper=grid_helper)
    fig.add_subplot(ax)

    # adjust axis
    # Left, right, top represent z, lookbacktime, RA respectively.
    # right axes is for look-back time yet to be coded
    ax.axis["left"].set_axis_direction("bottom")
    ax.axis["right"].set_axis_direction("top")
    ax.axis["bottom"].set_visible(False)
    

    ax.axis["top"].set_axis_direction("bottom")
    ax.axis["top"].toggle(ticklabels=True, label=True)
    ax.axis["top"].major_ticklabels.set_axis_direction("top")
    ax.axis["top"].label.set_axis_direction("top")

    ax.axis["left"].label.set_text(ylabel)
    ax.axis["top"].label.set_text(xlabel)
    
    ax.axis["top"].label.set_size(fontsize)
    ax.axis["left"].label.set_size(fontsize)
    
    ax.axis["top"].major_ticklabels.set_size(fontsize)
    ax.axis["left"].major_ticklabels.set_size(fontsize)
    
    # create a parasite axes that transData in RA, z
    aux = ax.get_aux_axes(tr)
    aux.patch = ax.patch    # for aux_ax to have a clip path as in ax
    ax.patch.zorder = 0    # but this has a side effect that the patch is
    # drawn twice, and possibly over some other
    # artists. So, we decrease the zorder a bit to
    # prevent this.
    aux.scatter(ra, z, marker='*', c='black', s=0.01, rasterized=True, alpha=0.4)
    
    return ax, aux, fig  