#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import desispec.io

import numpy as np
from   glob import glob
import pandas as pd
import pylab as pl
from   tqdm import tqdm
import os
import argparse
from   astropy.table import Table, vstack, join

from desitarget.sv3 import sv3_targetmask    # For SV3

from src.ccatalog import create


# In[ ]:


# parser = argparse.ArgumentParser()
# parser.add_argument("-r", "--release", help="DESI Release")
# parser.add_argument("-s", "--survey", help="Survey")
# parser.add_argument("-c", "--clss", help="Class (bgs,...)")
# parser.add_argument("-o", "--overwrite", help="Continue compilation y/n")
# args = parser.parse_args()

# release   = args.release
# survey    = args.survey
# clss      = args.clss
# overwrite = args.orverwrite


# In[ ]:


release   = 'fuji'
survey    = 'sv3'
clss      = 'bgs'
overwrite = 'no'

release_dir = desispec.io.specprod_root(release)

#- Read the exposures files
expo_data = Table.read(f'{release_dir}/exposures-{release}.fits', hdu='EXPOSURES')
expo_data = expo_data[expo_data['SURVEY']==survey]
expo_data = expo_data[(expo_data['PROGRAM']=='bright') | (expo_data['PROGRAM']=='dark')]

nights = expo_data['NIGHT']
tiles  = expo_data['TILEID']


# In[ ]:


#- Read the zpix files
zpix_cat = Table.read(f'{release_dir}/zcatalog/zall-pix-{release}.fits', hdu="ZCATALOG")

#- Define bgs, brigh and dark filters
is_bgs    = zpix_cat['SV3_DESI_TARGET'] & sv3_targetmask.desi_mask['BGS_ANY'] != 0
# print(sum(is_bgs))
is_bright = zpix_cat['PROGRAM'] == 'bright'
is_dark   = zpix_cat['PROGRAM'] == 'dark'

#- Select  bgs and brigh|dark 
targetids = zpix_cat[is_bgs & (is_bright | is_dark)]['TARGETID']
zpix      = zpix_cat[is_bgs & (is_bright | is_dark)]['Z']


# In[ ]:


df_targetids = pd.DataFrame({'TARGETID': pd.Series(targetids, dtype='int'),
                             'ZPIX'    : pd.Series(zpix, dtype='float')
                            })
df_targetids


# In[ ]:


_ = pl.hist(df_targetids['ZPIX'])


# In[ ]:


df_targetids = df_targetids[~df_targetids['TARGETID'].duplicated()]


# In[ ]:


#- Define targets from VAC
vac_path = '/global/cfs/cdirs/desi/public/edr/vac'
columns_spec = ['TARGETID','CONTINUUM_Z'] # 'Z','Z_RR','RA','DEC'
columns_photo = ['TARGETID','LOGL_5100','LOGMSTAR','ABSMAG_U','ABSMAG_B','ABSMAG_V',
                 'ABSMAG_SDSS_U','ABSMAG_SDSS_G','ABSMAG_SDSS_R','ABSMAG_SDSS_I','ABSMAG_SDSS_Z']


# In[ ]:


spec_bright = Table.read(f'{vac_path}/fastspecfit/{release}/v1.0/catalogs/fastspec-{release}-{survey}-bright.fits', hdu='FASTSPEC')[columns_spec]
spec_dark   = Table.read(f'{vac_path}/fastspecfit/{release}/v1.0/catalogs/fastspec-{release}-{survey}-dark.fits', hdu='FASTSPEC')[columns_spec]
# spec_keys = Table.read(f'{path}/fastspecfit/fuji/v1.0/catalogs/fastspec-fuji-sv3-bright.fits', hdu='FASTSPEC').keys()
spec = vstack([spec_bright, spec_dark])
del spec_bright, spec_dark

phot_bright = Table.read(f'{vac_path}/fastspecfit/{release}/v1.0/catalogs/fastphot-{release}-{survey}-bright.fits', hdu=1)[columns_photo]
phot_dark   = Table.read(f'{vac_path}/fastspecfit/{release}/v1.0/catalogs/fastphot-{release}-{survey}-dark.fits', hdu=1)[columns_photo]
# phot_keys = Table.read(f'{path}/fastspecfit/fuji/v1.0/catalogs/fastphot-fuji-sv3-bright.fits', hdu='FASTPHOT').keys()
phot = vstack([phot_bright, phot_dark])
del phot_bright, phot_dark

phys_targets = join(spec, phot, keys=['TARGETID']).to_pandas()
del spec, phot

#- Apply filter to remove empty data
phys_targets = phys_targets[(phys_targets['ABSMAG_B'] != 0) & (phys_targets['ABSMAG_U'] != 0) & (phys_targets['ABSMAG_V'] != 0)]
print(len(phys_targets))


# In[ ]:


phys_targets


# In[ ]:


# Match with bgs targets
print(len(phys_targets))
phys_targets = pd.merge(phys_targets, df_targetids, on=['TARGETID'])
print(len(phys_targets))
#- Remove duplicated entries
phys_targets = phys_targets.drop_duplicates()
print(len(phys_targets))

pbar_night = tqdm(total=len(nights), desc=f'{survey}')
for night, tile in zip(nights, tiles):
    create(release, survey, clss, phys_targets, night, tile, overwrite)
    pbar_night.update()
pbar_night.close()


# In[ ]:


_= pl.hist(phys_targets['CONTINUUM_Z'])


# In[ ]:


sum(phys_targets['CONTINUUM_Z']>0.5)


# In[ ]:




