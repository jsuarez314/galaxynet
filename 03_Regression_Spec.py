#!/usr/bin/env python

import numpy as np
import os 
import argparse
import pylab as pl
from   glob import glob
from   PIL import Image

import torch
from torch            import nn
from torch            import FloatTensor
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torch.nn         import functional as F
from torchvision      import transforms, models

from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error

pl.style.use('../plots.mplstyle')

def norm(x):
    return (x - np.mean(x) )/np.std(x)

def wnorm(x,w):
    std_weighted = np.sqrt(np.average((x-np.average(x, weights=w))**2, weights=w))
    x = (x - np.sum(x*w)/np.sum(w))/std_weighted    

def invnorm(x):
    return x

class ReadDataset(Dataset):

    def __init__(self, predictor_paths, target_paths):
        self.predictor_files = predictor_paths
        self.target_files    = target_paths
        self.weights_files   = [i.replace('flux','fluxivar') for i in predictor_paths]
    
    def __getitem__(self, index):
        x = np.load(self.predictor_files[index])
        
        if os.path.isfile(self.weights_files[index]):
            # print('Weigthed')
            w = np.load(self.weights_files[index])
            # x = wnorm(x,w)
            x = norm(x)
        else:
            # print('Not Weigthed')
            x = norm(x) 
       
        y = np.load(self.target_files[index])[0] #- Get only the first feature 0:Redshift
        
        x = FloatTensor(x).view(-1,len(x))
        y = torch.tensor(y).float()
        return x, y
    
    def __len__(self):
        return len(self.predictor_files)

class CNN(torch.nn.Module):
    def __init__(self, in_channels, out_channels, dropout):
        super().__init__()
        self.conv1d1 = torch.nn.Conv1d(in_channels, out_channels=out_channels, kernel_size=10, stride=2, padding=0)
        self.batch1 = torch.nn.BatchNorm1d(out_channels)           
        self.conv1d2 = torch.nn.Conv1d(in_channels=out_channels, out_channels=out_channels, kernel_size=10, stride=2, padding=0)
        self.batch2 = torch.nn.BatchNorm1d(out_channels)
        self.conv1d3 = torch.nn.Conv1d(in_channels=out_channels, out_channels=out_channels, kernel_size=10, stride=2, padding=0)
        self.batch3 = torch.nn.BatchNorm1d(out_channels)
        self.conv1d4 = torch.nn.Conv1d(in_channels=out_channels, out_channels=out_channels, kernel_size=10, stride=2, padding=0)
        self.batch4 = torch.nn.BatchNorm1d(out_channels)
        self.fc1 = torch.nn.Linear(48900, 580)
        self.dropout = torch.nn.Dropout(p=dropout)
        self.fc2 = torch.nn.Linear(580, 290)
        self.fc3 = torch.nn.Linear(290, 1)
        
    def forward(self, x):
        x = F.relu(self.batch1(self.conv1d1(x)))
        x = F.relu(self.batch2(self.conv1d2(x)))
        x = F.relu(self.batch3(self.conv1d3(x)))   
        x = F.relu(self.batch4(self.conv1d4(x)))
        x = torch.flatten(x,1)
        x = F.relu(self.fc1(x))
        x = self.dropout(x)
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        
        return x
    
def ResNet():
    model = nn.Sequential()
    model.add_module('resnet_block_1', resnet_block(in_channels=1, out_channels=32, num_residuals=5, stride=1))
    model.add_module('resnet_block_2', resnet_block(in_channels=32, out_channels=64, num_residuals=1, stride=1))
    model.add_module('resnet_block_3', resnet_block(in_channels=64, out_channels=32, num_residuals=1, stride=1))
    model.add_module('resnet_block_4', resnet_block(in_channels=32, out_channels=1, num_residuals=1, stride=1))
    model.add_module('flatten', nn.Flatten())
    model.add_module('fc1', nn.Sequential(nn.Linear(in_features=7958, out_features=796, bias=True), nn.ReLU()))
    model.add_module('fc2', nn.Sequential(nn.Linear(in_features=796, out_features=199, bias=True), nn.ReLU()))
    model.add_module('fc3', nn.Linear(in_features=199, out_features=1, bias=True))    
    return model    

def resnet_block(in_channels, out_channels, num_residuals, stride=1):
    blk = []
    for i in range(num_residuals):
        if i==0:
            blk.append(Residual(in_channels, out_channels, use_1x1conv=True, stride=stride))
        else:
            blk.append(Residual(out_channels, out_channels))
    return nn.Sequential(*blk)

class Residual(nn.Module):
    def __init__(self, in_channels, out_channels, use_1x1conv=False, stride=1):
        super(Residual, self).__init__()
        self.conv1 = nn.Conv1d(in_channels, out_channels, kernel_size=500, padding=250, stride=stride)
        self.conv2 = nn.Conv1d(out_channels, out_channels, kernel_size=200, padding=100, stride=stride)
        self.conv3 = nn.Conv1d(out_channels, out_channels, kernel_size=15, padding=6, stride=1)
        if use_1x1conv:
            self.conv4 = nn.Conv1d(in_channels, out_channels, kernel_size=1, stride=stride)
        else:
            self.conv4 = None
        self.bn1 = nn.BatchNorm1d(out_channels)
        self.bn2 = nn.BatchNorm1d(out_channels)
        self.bn3 = nn.BatchNorm1d(out_channels)
        #self.fc = nn.Linear(in_features, out_features, bias=True)
        
    def forward(self, x):
        y1 = F.relu(self.bn1(self.conv1(x)))
        y2 = F.relu(self.bn2(self.conv2(y1)))
        y3 = self.bn3(self.conv3(y2))
        if self.conv4:
            x = self.conv4(x)
        return F.relu(y3+x)
    

def main():
    #- Set parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--lr', type=float, default=1e-3,
                        help='Initial learning rate (default: 1e-3)')
    
    parser.add_argument('--epochs', type=int, default=300,
                        help='Maximum number of epochs (default: 300)')
    
    parser.add_argument('--batch', type=int, default=13,
                        help='Batch size (default: 13)')
    
    parser.add_argument('--name', type=str, default='Test',
                        help='Name of the current test (default: Test)')
    
    parser.add_argument('--kernel', type=int, default=10,
                        help='Kernel Size')

    parser.add_argument('--dropout', type=float, default=0.2, 
                        help='Dropout probability')
    
    parser.add_argument('--model', type=str, default='resnet',
                        help='cnn/resnet')
    
    parser.add_argument('--label', type=str, default='',
                        help='Label to compare runnings')    
    
    parser.add_argument('--data_path', type=str, default=None, 
                        help='Name of the folder with the data')
    
    
    #parser.add_argument('--load_model', type=str, default='best_f1', help='Weights to load (default: best_f1)')
    #parser.add_argument('--test', action='store_false', default=True, help='Only test the model')
    #parser.add_argument('--resume', action='store_true', default=False, help='Continue training a model')
    #parser.add_argument('--ft', action='store_true', default=False, help='Fine-tune a model')
    #parser.add_argument('--freeze', action='store_false', default=True, help='Freeze weights of the model')
    #parser.add_argument('--gpu', type=str, default='0', help='GPU(s) to use (default: 0)')
    #parser.add_argument('--amp', action='store_true', default=False,help='Train with automatic mixed precision')
    
    args = parser.parse_args()   
    
    save_path = os.path.join('train', args.name, args.label)
    if not os.path.isdir(save_path):
        os.makedirs(save_path, exist_ok=True)
    
    data_path = args.data_path
    #- Path to all the image files
    images = np.sort( glob(f'{data_path}/npy/*fluxbrz*.npy') )

    #- Filtered by redshift
    images_filtered_by_Z = []
    targets_filtered_by_Z = []
    Z_max=0.5
    
    for img in images:
        Z = np.load(img)[0] #- Get only the first feature 0:Redshift
        if Z < Z_max:
            images_filtered_by_Z.append(img)
            targets_filtered_by_Z.append(img.replace('fluxbrz','features'))
    
    #- Path to all the features files
    # targets = np.sort(glob(f'{data_path}/npy/*features*.npy'))
    targets = np.array([i.replace('fluzbrz','features') for i in images])

    # images_filtered_by_Z  = np.array(images)[:]
    # targets_filtered_by_Z = np.array(targets)[:]

    images_filtered_by_Z = np.array(images_filtered_by_Z)
    targets_filtered_by_Z = np.array(targets_filtered_by_Z)

    #- Number of objects
    N = len(images_filtered_by_Z)
    print(f'#####\n There are {N} objects\n#####')

    ii       = np.random.choice(range(N), N, replace=False)
    ii_train = ii[:int(N*.7)]
    ii_valid = ii[int(N*.7):int(N*.9)]
    ii_test  = ii[int(N*.9):]

    img_train_files = images_filtered_by_Z[ii_train]
    tgt_train_files = targets_filtered_by_Z[ii_train]

    img_valid_files = images_filtered_by_Z[ii_valid]
    tgt_valid_files = targets_filtered_by_Z[ii_valid]

    img_test_files = images_filtered_by_Z[ii_test]
    tgt_test_files = targets_filtered_by_Z[ii_test]

    print(f'#######\n Train Size: {len(img_train_files)}')
    print(f' Valid  Size: {len(img_valid_files)}')
    print(f' Test  Size: {len(img_test_files)}\n#######')

    train_dataset = ReadDataset(img_train_files, tgt_train_files)
    valid_dataset  = ReadDataset(img_valid_files,  tgt_valid_files)
    test_dataset  = ReadDataset(img_test_files,  tgt_test_files)

    # sample_idx = torch.randint(0, len(train_dataset), size=(9,)).detach().numpy()
    # figure = pl.figure(figsize=(8, 8))
    # cols, rows = 3, 3
    # for i in range(0, cols * rows):
    #     spec, tgt = train_dataset[sample_idx[i]]
    #     figure.add_subplot(rows, cols, i+1)
    #     pl.title(f'Z {tgt.item():.4f}')
    #     pl.plot(spec.squeeze())
    # print(f'Size {len(spec.squeeze())}')
    # pl.tight_layout()
    # pl.savefig()
    # pl.show()

    
    #- Define the batch_size
    batch_size = args.batch
    train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_dataloader  = DataLoader(test_dataset,  batch_size=batch_size , shuffle=True)
    valid_dataloader  = DataLoader(valid_dataset,  batch_size=batch_size , shuffle=True)
    
    # train_features, train_labels = next(iter(train_dataloader))
    #- Display image and label
    # spec = train_features[0].squeeze().detach().numpy()
    # label = train_labels[0].item()
    # pl.title(f'Z {label:.4f}')
    # pl.plot(spec)
    # pl.show()

    #- Define the device cpu/gpu
    device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
    print(device)

    #- Define the model
    if args.model == 'cnn':
        model = CNN(in_channels=1, out_channels=args.kernel, dropout=args.dropout)
    elif args.model == 'resnet':
        model = ResNet()
    else:
        print('This model doesnt exist')

    #- Send the model to the device
    model = model.to(device)
    
    #- Define the optimizer and the criterion
    optimizer = torch.optim.SGD(model.parameters(), lr=args.lr) 
    criterion = torch.nn.MSELoss()

    loss_train = []
    loss_valid  = []
    
    #- Running name
    name = args.name
    for epoch in range(args.epochs):
        loss_train.append( train(model, epoch, train_dataloader, device, optimizer, criterion) )
        loss_valid.append(  test(model, epoch, valid_dataloader, device, criterion) )

    fig = pl.figure(figsize=(8,4))
    pl.plot(loss_train, label='Train')
    pl.plot(loss_valid, label='Valid')
    pl.legend()
    pl.savefig(f'{save_path}/Loss_RegSpec_{name}.png', bbox_inches='tight')
    # pl.show()
        
    #- Put the model on test mode
    model.eval()
    with torch.no_grad():
        Y_pred = []
        Y_test = []
        for batch_idx, (data, target) in enumerate(test_dataloader):    
            data = FloatTensor(data)
            target = FloatTensor(target)
            data   = data.to(device)
            target = target.to(device)        
            Y_pred.append(model(data).cpu().squeeze().detach().numpy())
            Y_test.append(target.cpu().detach().numpy())
        Y_predtest = np.concatenate([np.ravel(i) for i in Y_pred])
        Y_test = np.concatenate([np.ravel(i) for i in Y_test])

        Y_pred = []
        Y_train = []    
        for batch_idx, (data, target) in enumerate(train_dataloader):    
            data = FloatTensor(data)
            target = FloatTensor(target).view(len(target), -1)
            data   = data.to(device)
            target = target.to(device)        
            Y_pred.append(model(data).cpu().squeeze().detach().numpy())
            Y_train.append(target.cpu().detach().numpy())
        Y_predtrain = np.concatenate([np.ravel(i) for i in Y_pred])
        Y_train = np.concatenate([np.ravel(i) for i in Y_train])

    fig = pl.figure(figsize=(20,5))
    pl.subplot(1,3,1)
    pl.scatter(Y_predtest, Y_test, alpha=0.5)
    maxx = max(max(Y_predtest),max(Y_test))
    pl.plot([0, maxx], [0, maxx], '--')
    pl.xlabel(r'$z_{pred}$')
    pl.ylabel(r'$z_{test}$')
    pl.title(r'$z_{pred}$ - $z_{test}$'+f'\n'+r'$r^2$'+f'={r2_score(Y_test, Y_predtest):.3f}   -    MSE={mean_squared_error(Y_test, Y_predtest):.4f}')
    pl.subplot(1,3,2)
    _ = pl.hist(Y_predtest - Y_test, label=r'$z_{pred}$ - $z_{test}$', alpha=0.5)
    pl.legend()
    pl.subplot(1,3,3)
    _ = pl.hist(Y_predtest, label='Pred', alpha=0.5)
    _ = pl.hist(Y_test, label='Test', alpha=0.5)
    pl.legend()
    pl.savefig(f'{save_path}/RegSpec_test_{name}.png', bbox_inches='tight')

    fig = pl.figure(figsize=(20,5))
    pl.subplot(1,3,1)
    pl.scatter(Y_predtrain, Y_train, alpha=0.5)
    maxx = max(max(Y_predtrain),max(Y_train))
    pl.plot([0, maxx], [0, maxx], '--')
    pl.xlabel(r'$z_{pred}$')
    pl.ylabel(r'$z_{train}$')
    pl.title(r'$z_{pred}$ - $z_{train}$'+f'\n'+r'$r^2$'+f'={r2_score(Y_train, Y_predtrain):.3f}   -    MSE={mean_squared_error(Y_train, Y_predtrain):.4f}')
    pl.subplot(1,3,2)
    _ = pl.hist(Y_predtrain - Y_train, label=r'$z_{pred}$ - $z_{train}$', alpha=0.5)
    pl.legend()
    pl.subplot(1,3,3)
    _ = pl.hist(Y_predtrain, label='Pred', alpha=0.5)
    _ = pl.hist(Y_train, label='train', alpha=0.5)
    pl.legend()
    pl.savefig(f'{save_path}/RegSpec_train_{name}.png', bbox_inches='tight')
        
def train(model, epoch, dataloader, device, optimizer, criterion):
    model.train()
    
    #- Start the train loop
    for batch_id, (X_batch, Y_batch) in enumerate(dataloader):
        
        #- Send data to device 
        X_batch = X_batch.to(device)
        Y_batch = Y_batch.to(device)

        #- Make the prediction
        Y_pred = model(X_batch).squeeze()

        #- Compute the loss
        loss = criterion(Y_pred, Y_batch)

        #- Back Propagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print(f'Epoch:{epoch+1} - Loss_Train:{loss.item():.4f}')        
    return loss.item()

def test(model, epoch, dataloader, device, criterion):
    model.eval()
    
    #- Start the test loop
    for batch_id, (X_batch, Y_batch) in enumerate(dataloader):
        
        #- Send data to device 
        X_batch = X_batch.to(device)
        Y_batch = Y_batch.to(device)

        #- Make the prediction
        Y_pred = model(X_batch).squeeze()

        #- Compute the loss
        loss = criterion(Y_pred, Y_batch)

    print(f'Epoch:{epoch+1} - Loss_Test:{loss.item():.4f}')        
    return loss.item()

if __name__ == '__main__':
    main()
