#!/usr/bin/env python

import numpy as np
import os 
import argparse
import pylab as pl
from   glob import glob
from   PIL import Image

import torch
from torch            import nn
from torch            import FloatTensor
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torch.nn         import functional as F
from torchvision      import transforms, models
from torchvision.models import resnet18, ResNet18_Weights
from torchvision.models import resnet50, ResNet50_Weights
from torchvision.models import resnet152, ResNet152_Weights

from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error

pl.style.use('../plots.mplstyle')

def norm(x):
    return (x - np.mean(x) )/np.std(x)

def wnorm(x,w):
    std_weighted = np.sqrt(np.average((x-np.average(x, weights=w))**2, weights=w))
    x = (x - np.sum(x*w)/np.sum(w))/std_weighted    

def tensor2img(tensor):
    mean=[0.485, 0.456, 0.406]
    std=[0.229, 0.224, 0.225]
    img_restore = tensor * torch.tensor(std).view(3, 1, 1) + torch.tensor(mean).view(3, 1, 1)
    return transforms.ToPILImage()(img_restore)

class ReadDataset(Dataset):

    def __init__(self, photo_paths, spec_paths, target_paths):
        self.photo_files = photo_paths
        self.spec_files = spec_paths
        self.target_files = target_paths
        
        self.photo_transforms = transforms.Compose([
            # transforms.Resize(256),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            ])
    
    def __getitem__(self, index):
        x1 = np.load(self.photo_files[index])
        x1 = self.photo_transforms(x1)
        
        x2 = np.load(self.spec_files[index])
        x2 = norm(x2)
        x2 = FloatTensor(x2).view(-1,len(x2))
        
        y = np.load(self.target_files[index])[0] #- Get only the first feature 0:Redshift
        y = torch.tensor(y).float()
        
        return x1, x2, y
    
    def __len__(self):
        return len(self.target_files)
    

class DeepGalaxyNet(torch.nn.Module):
    def __init__(self, pretrained=True, model='resnet50', dropout=0.3, out_channels=100):
        super().__init__()
                
        if model=='resnet18':
            model = resnet18(weights=ResNet18_Weights)
        elif model=='resnet50':
            model = resnet50(weights=ResNet50_Weights)        
        elif model=='resnet152':
            model = resnet152(weights=ResNet152_Weights)
        else:
            print('Other model doesnt exist')
        
        count = 0
        for child in model.children():  
            count += 1
            if count < 7:    
                for param in child.parameters():
                    param.requires_grad = False
                    
        model.load_state_dict(model.state_dict())     
            
        self.features = torch.nn.ModuleList(model.children())[:-1]
        self.features = torch.nn.Sequential(*self.features)
        
        self.photo_fc1 = torch.nn.Linear(model.fc.in_features, 512)
        self.photo_fc2 = torch.nn.Linear(512, 256)
       
        self.spec_conv1d1 = torch.nn.Conv1d(1, out_channels=out_channels, kernel_size=10, stride=2, padding=0)
        self.spec_batch1 = torch.nn.BatchNorm1d(out_channels)           
        self.spec_conv1d2 = torch.nn.Conv1d(in_channels=out_channels, out_channels=out_channels, kernel_size=10, stride=2, padding=0)
        self.spec_batch2 = torch.nn.BatchNorm1d(out_channels)
        self.spec_conv1d3 = torch.nn.Conv1d(in_channels=out_channels, out_channels=out_channels, kernel_size=10, stride=2, padding=0)
        self.spec_batch3 = torch.nn.BatchNorm1d(out_channels)
        self.spec_conv1d4 = torch.nn.Conv1d(in_channels=out_channels, out_channels=out_channels, kernel_size=10, stride=2, padding=0)
        self.spec_batch4 = torch.nn.BatchNorm1d(out_channels)
        self.spec_fc1 = torch.nn.Linear(48900, 580)
        self.spec_fc2 = torch.nn.Linear(580, 290)

        self.fc3 = torch.nn.Linear(256+290, 200)
        self.dropout = torch.nn.Dropout(p=dropout)        
        self.fc4 = torch.nn.Linear(200, 200)
        self.fc5 = torch.nn.Linear(200, 1)
        
        
    def forward(self, img, spc):
        ## Model on Photo
        #-Call Features
        img = self.features(img)
        #-Flatten
        img = img.view(img.size(0), -1)
        # and also our new layers 
        img = F.relu(self.photo_fc1(img))
        img = F.relu(self.photo_fc2(img))
        
        #### Model on Spec
        spc = F.relu(self.spec_batch1(self.spec_conv1d1(spc)))
        spc = F.relu(self.spec_batch2(self.spec_conv1d2(spc)))
        spc = F.relu(self.spec_batch3(self.spec_conv1d3(spc)))   
        spc = F.relu(self.spec_batch4(self.spec_conv1d4(spc)))
        spc = torch.flatten(spc,1)
        spc = F.relu(self.spec_fc1(spc))
        spc = F.relu(self.spec_fc2(spc))
        
        ### Combine
        x = F.relu(self.fc3(torch.cat([img, spc], dim=1)))
        x = self.dropout(x)
        x = F.relu(self.fc4(x))
        x = self.fc5(x)
        
        return x
    

def main():
    #- Set parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--lr', type=float, default=1e-3,
                        help='Initial learning rate (default: 1e-3)')
    
    parser.add_argument('--epochs', type=int, default=300,
                        help='Maximum number of epochs (default: 300)')
    
    parser.add_argument('--batch', type=int, default=13,
                        help='Batch size (default: 13)')
    
    parser.add_argument('--name', type=str, default='Test',
                        help='Name of the current test (default: Test)')
    
    parser.add_argument('--out_channels', type=int, default=100,
                        help='Kernel Size')

    parser.add_argument('--dropout', type=float, default=0.2, 
                        help='Dropout probability')
    
    parser.add_argument('--model', type=str, default='resnet',
                        help='cnn/resnet')
    
    parser.add_argument('--label', type=str, default='',
                        help='Label to compare runnings')    
    
    parser.add_argument('--data_path', type=str, default=None, 
                        help='Name of the folder with the data')
    
    
    #parser.add_argument('--load_model', type=str, default='best_f1', help='Weights to load (default: best_f1)')
    #parser.add_argument('--test', action='store_false', default=True, help='Only test the model')
    #parser.add_argument('--resume', action='store_true', default=False, help='Continue training a model')
    #parser.add_argument('--ft', action='store_true', default=False, help='Fine-tune a model')
    #parser.add_argument('--freeze', action='store_false', default=True, help='Freeze weights of the model')
    #parser.add_argument('--gpu', type=str, default='0', help='GPU(s) to use (default: 0)')
    #parser.add_argument('--amp', action='store_true', default=False,help='Train with automatic mixed precision')
    
    args = parser.parse_args()   
    
    save_path = os.path.join('train', args.name, args.label)
    if not os.path.isdir(save_path):
        os.makedirs(save_path, exist_ok=True)
    
    data_path = args.data_path
    #- Path to all the image files
    images = np.sort(glob(f'{data_path}/npy/*photogrz*.npy') )
    spects = np.array([i.replace('photogrz','fluxbrz') for i in images])
    targets = np.array([i.replace('photogrz','features') for i in images])

    #- Filtered by redshift
    images_filtered_by_Z = []
    spects_filtered_by_Z = []
    targets_filtered_by_Z = []
    Z_max=0.5

    for img, spc, tgt in zip(images, spects, targets):
        Z = np.load(tgt)[0] #- Get only the first feature 0:Redshift
        if Z < Z_max:
            images_filtered_by_Z.append(img)
            spects_filtered_by_Z.append(spc)
            targets_filtered_by_Z.append(tgt)

    images = np.array(images_filtered_by_Z)
    spects = np.array(spects_filtered_by_Z)
    targets = np.array(targets_filtered_by_Z)

    #- Number of objects
    N = len(images)
    print(f'#####\n There are {N} objects\n#####')

    ii       = np.random.choice(range(N), N, replace=False)
    ii_train = ii[:int(N*.7)]
    ii_valid = ii[int(N*.7):int(N*.9)]
    ii_test  = ii[int(N*.9):]

    img_train_files = images[ii_train]
    spc_train_files = spects[ii_train]
    tgt_train_files = targets[ii_train]

    img_valid_files = images[ii_valid]
    spc_valid_files = spects[ii_valid]
    tgt_valid_files = targets[ii_valid]

    img_test_files = images[ii_test]
    spc_test_files = spects[ii_test]
    tgt_test_files = targets[ii_test]

    print(f'#####\n Train Size: {len(img_train_files)}')
    print(f'Valid Size: {len(img_valid_files)}')
    print(f'Test Size:{len(img_test_files)}\n#####')

    train_dataset = ReadDataset(img_train_files, spc_train_files, tgt_train_files)
    valid_dataset = ReadDataset(img_valid_files, spc_valid_files, tgt_valid_files)
    test_dataset  = ReadDataset(img_test_files, spc_test_files, tgt_test_files)

    # sample_idx = torch.randint(0, len(train_dataset), size=(9,)).detach().numpy()
    # figure = pl.figure(figsize=(8, 8))
    # cols, rows = 3, 3
    # for i in range(0, cols * rows):
    #     spec, tgt = train_dataset[sample_idx[i]]
    #     figure.add_subplot(rows, cols, i+1)
    #     pl.title(f'Z {tgt.item():.4f}')
    #     pl.plot(spec.squeeze())
    # print(f'Size {len(spec.squeeze())}')
    # pl.tight_layout()
    # pl.savefig()
    # pl.show()
    
    #- Define the batch_size
    batch_size = args.batch
    train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_dataloader  = DataLoader(test_dataset,  batch_size=batch_size , shuffle=True)
    valid_dataloader  = DataLoader(valid_dataset,  batch_size=batch_size , shuffle=True)
    
    # train_features, train_labels = next(iter(train_dataloader))
    #- Display image and label
    # spec = train_features[0].squeeze().detach().numpy()
    # label = train_labels[0].item()
    # pl.title(f'Z {label:.4f}')
    # pl.plot(spec)
    # pl.show()

    #- Define the device cpu/gpu
    device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
    print(device)

    #- Define the model
    model = DeepGalaxyNet(model=args.model, dropout=args.dropout, out_channels=args.out_channels)

    #- Send the model to the device
    model = model.to(device)
    
    #- Define the optimizer and the criterion
    optimizer = torch.optim.SGD(model.parameters(), lr=args.lr) 
    criterion = torch.nn.MSELoss()

    loss_train = []
    loss_valid  = []
    
    #- Running name
    name = args.name
    for epoch in range(args.epochs):
        loss_train.append( train(model, epoch, train_dataloader, device, optimizer, criterion) )
        loss_valid.append(  test(model, epoch, valid_dataloader, device, criterion) )

    fig = pl.figure(figsize=(8,4))
    pl.plot(loss_train, label='Train')
    pl.plot(loss_valid, label='Valid')
    pl.legend()
    pl.savefig(f'{save_path}/Loss_RegSpec_{name}.png', bbox_inches='tight')
    # pl.show()
        
    #- Put the model on test mode
#- Put the model on test mode
    model.eval()
    with torch.no_grad():
        Y_pred = []
        Y_test = []
        for batch_idx, (data_phot, data_spec, target) in enumerate(test_dataloader):    
            data_phot = FloatTensor(data_phot)
            data_spec = FloatTensor(data_spec)
            target = FloatTensor(target)

            data_phot   = data_phot.to(device)
            data_spec   = data_spec.to(device)
            target = target.to(device)

            Y_pred.append(model(data_phot, data_spec).cpu().squeeze().detach().numpy())
            Y_test.append(target.cpu().detach().numpy())    
        Y_predtest = np.concatenate([np.ravel(i) for i in Y_pred])
        Y_test = np.concatenate([np.ravel(i) for i in Y_test])

        Y_pred = []
        Y_train = []    
        for batch_idx, (data_phot, data_spec, target) in enumerate(train_dataloader):    
            data_phot = FloatTensor(data_phot)
            data_spec = FloatTensor(data_spec)
            target = FloatTensor(target)

            data_phot   = data_phot.to(device)
            data_spec   = data_spec.to(device)
            target = target.to(device)

            Y_pred.append(model(data_phot, data_spec).cpu().squeeze().detach().numpy())
            Y_train.append(target.cpu().detach().numpy())
        Y_predtrain = np.concatenate([np.ravel(i) for i in Y_pred])
        Y_train = np.concatenate([np.ravel(i) for i in Y_train])

    fig = pl.figure(figsize=(20,5))
    pl.subplot(1,3,1)
    pl.scatter(Y_predtest, Y_test, alpha=0.5)
    maxx = max(max(Y_predtest),max(Y_test))
    pl.plot([0, maxx], [0, maxx], '--')
    pl.xlabel(r'$z_{pred}$')
    pl.ylabel(r'$z_{test}$')
    pl.title(r'$z_{pred}$ - $z_{test}$'+f'\n'+r'$r^2$'+f'={r2_score(Y_test, Y_predtest):.3f}   -    MSE={mean_squared_error(Y_test, Y_predtest):.4f}')
    pl.subplot(1,3,2)
    _ = pl.hist(Y_predtest - Y_test, label=r'$z_{pred}$ - $z_{test}$', alpha=0.5)
    pl.legend()
    pl.subplot(1,3,3)
    _ = pl.hist(Y_predtest, label='Pred', alpha=0.5)
    _ = pl.hist(Y_test, label='Test', alpha=0.5)
    pl.legend()
    pl.savefig(f'{save_path}/RegSpec_test_{name}.png', bbox_inches='tight')

    fig = pl.figure(figsize=(20,5))
    pl.subplot(1,3,1)
    pl.scatter(Y_predtrain, Y_train, alpha=0.5)
    maxx = max(max(Y_predtrain),max(Y_train))
    pl.plot([0, maxx], [0, maxx], '--')
    pl.xlabel(r'$z_{pred}$')
    pl.ylabel(r'$z_{train}$')
    pl.title(r'$z_{pred}$ - $z_{train}$'+f'\n'+r'$r^2$'+f'={r2_score(Y_train, Y_predtrain):.3f}   -    MSE={mean_squared_error(Y_train, Y_predtrain):.4f}')
    pl.subplot(1,3,2)
    _ = pl.hist(Y_predtrain - Y_train, label=r'$z_{pred}$ - $z_{train}$', alpha=0.5)
    pl.legend()
    pl.subplot(1,3,3)
    _ = pl.hist(Y_predtrain, label='Pred', alpha=0.5)
    _ = pl.hist(Y_train, label='train', alpha=0.5)
    pl.legend()
    pl.savefig(f'{save_path}/RegSpec_train_{name}.png', bbox_inches='tight')
        
def train(model, epoch, dataloader, device, optimizer, criterion):
    model.train()
    
    #- Start the train loop
    for batch_id, (X1_batch, X2_batch, Y_batch) in enumerate(dataloader):
        
        #- Send data to device 
        X1_batch = X1_batch.to(device)
        X2_batch = X2_batch.to(device)
        Y_batch = Y_batch.to(device)

        #- Make the prediction
        Y_pred = model(X1_batch, X2_batch).squeeze()

        #- Compute the loss
        loss = criterion(Y_pred, Y_batch)

        #- Back Propagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print(f'Epoch:{epoch+1} - Loss_Train:{loss.item():.4f}')        
    return loss.item()

def test(model, epoch, dataloader, device, criterion):
    model.eval()
    
    #- Start the test loop
    for batch_id, (X1_batch, X2_batch, Y_batch) in enumerate(dataloader):
        
        #- Send data to device 
        X1_batch = X1_batch.to(device)
        X2_batch = X2_batch.to(device)
        Y_batch = Y_batch.to(device)

        #- Make the prediction
        Y_pred = model(X1_batch, X2_batch).squeeze()

        #- Compute the loss
        loss = criterion(Y_pred, Y_batch)

    print(f'Epoch:{epoch+1} - Loss_Test:{loss.item():.4f}')        
    return loss.item()

if __name__ == '__main__':
    main()
