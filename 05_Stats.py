#!/usr/bin/env python
import numpy as np
import os
import argparse
import pylab as pl
import pandas as pd
from   glob import glob
from   PIL import Image
from   tqdm import tqdm

pl.style.use('../plots.mplstyle')

catalog = 'catalog_guadalupe_main_bgs'

#path   = f'../../data/{catalog}'
path   = f'./{catalog}'

images     = glob(f'{path}/npy/*photogrz*')
spectra    = [i.replace('photogrz','fluxbrz') for i in images]
features   = [i.replace('photogrz','features') for i in images]

print(len(images), len(features))

Z    = []
LogL = []
LogM = []
AbsMagU = []
AbsMagB = []
AbsMagV = []
AbsMag_sdss_U = []
AbsMag_sdss_G = []
AbsMag_sdss_R = []
AbsMag_sdss_I = []
AbsMag_sdss_Z = []

Z_lim = 0.6

img_filtered_by_Z = []
spc_filtered_by_Z = []
ftr_filtered_by_Z = []

Z_filtered_by_Z   = []
LogL_filtered_by_Z = []
LogM_filtered_by_Z = []
AbsMagU_filtered_by_Z = []
AbsMagB_filtered_by_Z = []
AbsMagV_filtered_by_Z = []
AbsMag_sdss_U_filtered_by_Z = []
AbsMag_sdss_G_filtered_by_Z = []
AbsMag_sdss_R_filtered_by_Z = []
AbsMag_sdss_I_filtered_by_Z = []
AbsMag_sdss_Z_filtered_by_Z = []

pbar = tqdm(total=len(images))
for img, spc, ftr in zip(images, spectra, features):
    Z_temp, LogL_temp, LogM_temp, u_absmag, b_absmag, v_absmag, u_sdss_absmag, g_sdss_absmag, r_sdss_absmag, i_sdss_absmag, z_sdss_absmag, zfix = np.load(ftr)
    Z.append(Z_temp)
    LogL.append(LogL_temp)
    LogM.append(LogM_temp)
    AbsMagU.append(u_absmag)
    AbsMagB.append(b_absmag)
    AbsMagV.append(v_absmag)
    AbsMag_sdss_U.append(u_sdss_absmag)
    AbsMag_sdss_G.append(g_sdss_absmag)
    AbsMag_sdss_R.append(r_sdss_absmag)
    AbsMag_sdss_I.append(i_sdss_absmag)
    AbsMag_sdss_Z.append(z_sdss_absmag)
    
    if Z_temp < Z_lim:
        Z_filtered_by_Z.append(Z_temp)
        spc_filtered_by_Z.append(spc)
        img_filtered_by_Z.append(img)
        ftr_filtered_by_Z.append(ftr)
        LogL_filtered_by_Z.append(LogL_temp)
        LogM_filtered_by_Z.append(LogM_temp)
        AbsMagU_filtered_by_Z.append( u_absmag)
        AbsMagB_filtered_by_Z.append( b_absmag)
        AbsMagV_filtered_by_Z.append( v_absmag)
        AbsMag_sdss_U_filtered_by_Z.append(u_sdss_absmag)
        AbsMag_sdss_G_filtered_by_Z.append(g_sdss_absmag)
        AbsMag_sdss_R_filtered_by_Z.append(r_sdss_absmag)
        AbsMag_sdss_I_filtered_by_Z.append(i_sdss_absmag)
        AbsMag_sdss_Z_filtered_by_Z.append(z_sdss_absmag)
    pbar.update()
pbar.close()
        
columns = ['IMAGE_FILE','CONTINUUM_Z', 'LOGL_5100', 'LOGMSTAR', 'ABSMAG_U', 'ABSMAG_B', 'ABSMAG_V','ABSMAG_SDSS_U','ABSMAG_SDSS_G','ABSMAG_SDSS_R', 'ABSMAG_SDSS_I', 'ABSMAG_SDSS_Z','MODEL_LABEL']

N = len(images)
ii       = np.random.choice(range(N), N, replace=False)
ii_train = ii[:int(N*.7)]
ii_valid = ii[int(N*.7):int(N*.9)]
ii_test  = ii[int(N*.9):]

#-  0:train    1:valid     2:test
ModelLabel = np.zeros(N)
ModelLabel[ii_train] = 0
ModelLabel[ii_valid] = 1
ModelLabel[ii_test]  = 2


pd_full = pd.DataFrame(np.c_[images, Z, LogL, LogM, AbsMagU, AbsMagB, AbsMagV, AbsMag_sdss_U, AbsMag_sdss_G, AbsMag_sdss_R, AbsMag_sdss_I, AbsMag_sdss_Z, ModelLabel], columns=columns)
pd_full.to_csv('guadalupe_stats_full.csv')




N = len(img_filtered_by_Z)
ii       = np.random.choice(range(N), N, replace=False)
ii_train = ii[:int(N*.7)]
ii_valid = ii[int(N*.7):int(N*.9)]
ii_test  = ii[int(N*.9):]

#-   0:train   1:valid   2:test
ModelLabel = np.zeros(N)
ModelLabel[ii_train] = 0
ModelLabel[ii_valid] = 1
ModelLabel[ii_test]  = 2 

pd_Zlim = pd.DataFrame(np.c_[img_filtered_by_Z, Z_filtered_by_Z, LogL_filtered_by_Z, LogM_filtered_by_Z, AbsMagU_filtered_by_Z, AbsMagB_filtered_by_Z, AbsMagV_filtered_by_Z, AbsMag_sdss_U_filtered_by_Z, AbsMag_sdss_G_filtered_by_Z, AbsMag_sdss_R_filtered_by_Z, AbsMag_sdss_I_filtered_by_Z, AbsMag_sdss_Z_filtered_by_Z, ModelLabel], columns=columns)
pd_Zlim.to_csv(f'guadalupe_stats_Z{Z_lim}.csv')
