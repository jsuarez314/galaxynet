#!/usr/bin/env python
# coding: utf-8

import os
from IPython. display import clear_output

import desispec.io
from   desitarget.targetmask import desi_mask
                                                                   
import numpy as np
import pylab as pl
import pandas as pd

import argparse
from   tqdm import tqdm

from   glob import glob
from   astropy.table import Table, vstack, hstack, join, unique

from   astropy.io import fits
import PIL

from   src.ccatalog import create

from   src.utils import mw_transmission
from   src.utils import mag
from   src.utils import ratioBA

def photo_error(photoerr_file, band, tid, target_ra, target_dec):
    # Function to write an error from the photometric data pushing proccess
    photoerror_files = pd.read_csv(photoerr_file)
    print(f'Error pulling photometric for {band} band')
    photoerror_files = photoerror_files.append(pd.DataFrame([[band, tid, target_ra, target_dec]],
                                                            columns=['band','targetid','target_ra','target_dec']))
    photoerror_files.to_csv(photoerr_file, index=False)
    del photoerror_files
#------------------------------------------


parser = argparse.ArgumentParser()
parser.add_argument("-r", "--release", help="DESI Release")
parser.add_argument("-s", "--survey", help="Survey")
parser.add_argument("-c", "--clss", help="Class: bgs,...")
parser.add_argument("-o", "--overwrite", help="Continue compilation True/False")
parser.add_argument("-n", "--nodes", help="Number of nodes")
parser.add_argument("-b", "--block", help="Block to run multiple calls")
args = parser.parse_args()

release   = args.release
survey    = args.survey
clss      = args.clss
overwrite = args.overwrite
block     = int(args.block)
nodes     = int(args.nodes)
#------------------------------------------


#------------------------------------------
#- Creating dataset folders
outpath = f'/pscratch/sd/j/jfsuarez/data/deepgn/catalog_{release}_{survey}_{clss}_jpg'
os.makedirs(f'{outpath}/photo/train/', exist_ok=True)
os.makedirs(f'{outpath}/photo/test/', exist_ok=True)
os.makedirs(f'{outpath}/photo/valid/', exist_ok=True)

photo_file = f'{outpath}/photo_features_full.csv'

if os.path.isfile(photo_file):
    print('Reading photo file...')
    df_targets = Table.read(photo_file)
else:
    #- Read the zpix files
    release_dir = desispec.io.specprod_root(release)
    print(f'{release_dir}/zcatalog/zall-pix-{release}.fits')
    # zpix_cat = Table.read(f'{release_dir}/zcatalog/zall-pix-{release}.fits', hdu="ZCATALOG")
    zpix_cat = Table.read(f'{release_dir}/zcatalog/ztile-main-bright-cumulative.fits', hdu="ZCATALOG")

    #- Filter BGS
    ii = ( (zpix_cat['BGS_TARGET'] != 0)
          &(zpix_cat['COADD_FIBERSTATUS'] == 0)
          &(zpix_cat['DELTACHI2'] > 40.0)
          &(zpix_cat['SPECTYPE'] == 'GALAXY')
          &(zpix_cat['ZERR'] < (5e-4*zpix_cat['Z']))
         )

    #- Define ZPIX Columns
    zpix_columns = ['TARGETID','Z','TARGET_RA','TARGET_DEC',
                    'FLUX_G','FLUX_R','FLUX_Z','FLUX_W1','FLUX_W2','EBV',
                    'SHAPE_R','SHAPE_E1','SHAPE_E2']

    #- Select the BGS and selected columns
    df_targets = zpix_cat[zpix_columns][ii]
    N_BGS = len(df_targets)
    df_targets

    ii_shuffle  = np.random.choice(N_BGS, N_BGS, replace=False)

    labelclass = np.zeros(N_BGS, dtype='object')
    labelclass[:int(N_BGS*.7)] = 'train'
    labelclass[int(N_BGS*.7):int(N_BGS*.9)] = 'test'
    labelclass[int(N_BGS*.9):] = 'valid'

    labelTab = Table()
    labelTab['LABEL'] = labelclass[ii_shuffle]
    labelTab['TARGETID'] = df_targets['TARGETID']

    df_targets = join(df_targets, labelTab , keys=['TARGETID'])

    # #- Apply filter to remove empty data
    df_targets = df_targets[(df_targets['FLUX_G'] != 0) & (df_targets['FLUX_R'] != 0) & 
                            (df_targets['FLUX_Z'] != 0) & (df_targets['FLUX_W1'] != 0) & 
                            (df_targets['FLUX_W2'] != 0) & (df_targets['SHAPE_R'] != 0) &
                            (df_targets['SHAPE_E1'] != 0) & (df_targets['SHAPE_E2'] != 0)
                           ]

    df_targets['RATIO_BA'] = ratioBA(df_targets['SHAPE_E1'], df_targets['SHAPE_E2'])

    df_targets.write(photo_file)

photoerr_file = f'./catalog_{release}_{survey}_{clss}_jpg_photoerr.csv'

#- If overwrite == True the catalog creation start from 0%
if overwrite == True:
    photoerror_files = pd.DataFrame(columns=['band','targetid','target_ra','target_dec'])
    photoerror_files.to_csv(photoerr_file, index=False)
    
N_BGS = len(df_targets)
print(f'BGS: {N_BGS}')

tid        = df_targets['TARGETID']
target_ra  = df_targets['TARGET_RA']
target_dec = df_targets['TARGET_DEC']
label      = df_targets['LABEL']
flux_g     = df_targets['FLUX_G']
flux_r     = df_targets['FLUX_R']
flux_z     = df_targets['FLUX_Z']
flux_w1    = df_targets['FLUX_W1']
flux_w2    = df_targets['FLUX_W2']
ebv        = df_targets['EBV']
ratio_ba   = df_targets['RATIO_BA']
shape_r    = df_targets['SHAPE_R']

AA = np.array([0,N_BGS])
BB = np.linspace(AA[0],AA[1], nodes+1, dtype=int) # To run by a uniquer node use 2 and block==0
NN = len(BB)-1
CC = np.zeros(NN, dtype=object)
for i in range(NN):
    CC[i] = np.array([BB[i],BB[i+1]])
print(CC[block])

pbar_photo = tqdm(total=CC[block][1]-CC[block][0])
for i in range(CC[block][0],CC[block][1]):
   
    if float(target_dec[i])>0:
        filename = f'{outpath}/photo/{label[i]}/DESI_{tid[i]}_{target_ra[i]}+{target_dec[i]}.fits'
    else:
        filename = f'{outpath}/photo/{label[i]}/DESI_{tid[i]}_{target_ra[i]}{target_dec[i]}.fits'

    #- If the file exists, then do nothing. Else download the images and create the fits file
    if os.path.isfile(filename):
        None
    else:
        command_g = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_g{block}.jpg --ra {target_ra[i]} --dec {target_dec[i]} --size 256 --layer ls-dr9 --pixscale 0.262 --band g --force'
        print(command_g)
        os.system(command_g)
        if os.path.isfile(f'temp_g{block}.jpg'):
            image_g = np.array(PIL.Image.open(f'temp_g{block}.jpg'))
            os.system(f'rm temp_g{block}.jpg')
        else:
            photo_error(photoerr_file, 'G', tid[i], target_ra[i], target_dec[i])

        command_r = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_r{block}.jpg --ra {target_ra[i]} --dec {target_dec[i]} --size 256 --layer ls-dr9 --pixscale 0.262 --band r --force'
        # print(command_r)
        os.system(command_r)
        if os.path.isfile(f'temp_r{block}.jpg'):
            image_r = np.array(PIL.Image.open(f'temp_r{block}.jpg'))
            os.system(f'rm temp_r{block}.jpg')
        else:
            photo_error(photoerr_file, 'R', tid[i], target_ra[i], target_dec[i])
            
        command_z = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_z{block}.jpg --ra {target_ra[i]} --dec {target_dec[i]} --size 256 --layer ls-dr9 --pixscale 0.262 --band z --force'
        # print(command_z)
        os.system(command_z)
        if os.path.isfile(f'temp_z{block}.jpg'):
            image_z = np.array(PIL.Image.open(f'temp_z{block}.jpg'))
            os.system(f'rm temp_z{block}.jpg')
        else:
            photo_error(photoerr_file, 'Z', tid[i], target_ra[i], target_dec[i])
        
        command_grz = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_grz{block}.jpg --ra {target_ra[i]} --dec {target_dec[i]} --size 256 --layer ls-dr9 --pixscale 0.262 --band grz --force'
        # print(command_grz)
        os.system(command_grz)
        if os.path.isfile(f'temp_grz{block}.jpg'):
            image_grz = np.array(PIL.Image.open(f'temp_grz{block}.jpg'))
            os.system(f'rm temp_grz{block}.jpg')
        else:
            photo_error(photoerr_file, 'GRZ', tid[i], target_ra[i], target_dec[i])

        command_w1 = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_w1{block}.jpg --ra {target_ra[i]} --dec {target_dec[i]} --size 256 --layer unwise-neo7 --pixscale 0.262 --band W1 --force'
        # print(command_w1)
        os.system(command_w1)
        if os.path.isfile(f'temp_w1{block}.jpg'):
            image_w1 = np.array(PIL.Image.open(f'temp_w1{block}.jpg'))
            os.system(f'rm temp_w1{block}.jpg')
        else:
            photo_error(photoerr_file, 'W1', tid[i], target_ra[i], target_dec[i])

        command_w2 = f'shifter --image dstndstn/viewer-cutouts cutout --output temp_w2{block}.jpg --ra {target_ra[i]} --dec {target_dec[i]} --size 256 --layer unwise-neo7 --pixscale 0.262 --band W2 --force'
        # print(command_w2)
        os.system(command_w2)
        if os.path.isfile(f'temp_w2{block}.jpg'):
            image_w2 = np.array(PIL.Image.open(f'temp_w2{block}.jpg'))
            os.system(f'rm temp_w2{block}.jpg')
        else:
            photo_error(photoerr_file, 'W2', tid[i], target_ra[i], target_dec[i])

            
        #-- Create the FITS file
        try:
            hdict  = {'TARGETID':tid[i], 'FLUX_G':flux_g[i], 'FLUX_R':flux_r[i], 'FLUX_Z':flux_z[i], 'FLUX_W1':flux_w1[i], 'FLUX_W2':flux_w2[i], 'EBV':ebv[i], 'SHAPE_R':shape_r[i], 'RATIO_BA':ratio_ba[i]}
            header = fits.Header(hdict)

            phdu    = fits.PrimaryHDU(header=header)
            hdu_g   = fits.ImageHDU(data=image_g)
            hdu_r   = fits.ImageHDU(data=image_r)
            hdu_z   = fits.ImageHDU(data=image_z)
            hdu_grz = fits.ImageHDU(data=image_grz)
            hdu_w1  = fits.ImageHDU(data=image_w1)
            hdu_w2  = fits.ImageHDU(data=image_w2)

            hdul = fits.HDUList([phdu, hdu_g, hdu_r, hdu_z, hdu_grz, hdu_w1, hdu_w2])
            hdul.writeto(filename)
        except:
            None
    pbar_photo.update()
pbar_photo.close()